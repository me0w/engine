from django.contrib import admin

# Register your models here.
from .models import Board, Post, Report, Ban, BannedFile

admin.site.register(Board)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    date_hierarchy = 'time'
    search_fields = ['ip', 'body']
    list_display = ('id', 'ip', 'body_nomarkup')
    list_filter = ('board',)


class PostsInline(admin.TabularInline):
    model = Report.post.through


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('reason',)
    exclude = ('post',)
    inlines = [
        PostsInline,
    ]


@admin.register(Ban)
class BanAdmin(admin.ModelAdmin):
    search_fields = ['ip_start', 'ip_end', 'reason']
    list_display = ('ip_start', 'ip_end', 'reason')
    date_hierarchy = 'created'
    list_filter = ('board',)


@admin.register(BannedFile)
class BannedFileAdmin(admin.ModelAdmin):
    list_display = ('hash',)
