import os
import re
import random

from math import floor, log

from django import template
import simplejson as json
from config.settings import config, STATIC_ROOT

register = template.Library()


@register.filter(name='to_dict')
def to_dict(json_string):
    if isinstance(json_string, str):
        return json.loads(json_string)


# return files sorted by the order of uploading
@register.filter(name='get_files')
def get_files(json_string):
    if isinstance(json_string, str):
        # don't even ask
        f = json.loads(json_string)
        try:
            f.sort(key=lambda x: int(''.join(re.findall(r'\d*', x['file']))))
            return f
        except ValueError:
            pass


@register.filter(name='first_file')
def first_file(json_string):
    if isinstance(json_string, str) and json_string != '':
        data = json.loads(json_string)
        if len(data):
            try:
                return data[0]['thumb']
            except KeyError:
                return 'None'


@register.filter(name='extension')
def extension(filename):
    if isinstance(filename, str):
        return filename.split('.')[-1]


@register.filter(name='to_time')
def to_time(date_time):
    return date_time.strftime(config['post_date'])


@register.simple_tag
def random_logo():
    return random.choice(os.listdir(STATIC_ROOT+'randlogo/'))


@register.filter(name='format_size')
def format_size(size_in_bytes):
    if size_in_bytes == 0:
        return "0B"
    size_name = ("", "K", "M", "G")
    index = int(floor(log(size_in_bytes, 1024)))
    p = pow(1024, index)
    s = round(size_in_bytes / p, 2)
    return f"{s} {size_name[index]}B"


@register.filter(name='get_flag')
def get_flag(body):
    flag_name = re.search(r"<tinyboard flag>(?P<flag>\w+)</tinyboard>", body)
    if flag_name:
        flag = flag_name.group('flag')
    else:
        flag = 'a1'
    return f'flags/{flag.lower()}.png'


@register.filter(name='clean')
def clean(body):
    patterns = [r"&lt;tinyboard (flag|proxy)&gt;.*&lt;\/tinyboard&gt;",
                r"<br ?/>"]
    for pattern in patterns:
        body = re.sub(pattern, ' ', body)
    return body


@register.filter(name='truncate')
def truncate(string):
    return string.split('\n')[0] if isinstance(truncate, str) else ''

