from django.conf.urls import url, include
from .views import render_board, render_catalog, render_index, render_thread
from .views import get_media, render_stat
from .api import board_threads_api, board_page_api, board_thread_api

urlpatterns = [
    url(r'^$', render_index, name="index"),
    url(r'^statistics/$', render_stat, name="statistics"),
    url(r'^(?P<board_name>[A-Za-z]+)/res/(?P<thread_id>\d+).html$',
        render_thread, name="thread"),
    url(r'^(?P<board_name>[A-Za-z]+)/$', render_board, name="board"),
    # API START
    url(
        r'^(?P<board_name>[A-Za-z]+)/threads.json$',
        board_threads_api, name="board_threads_api"
    ),
    url(
        r'^(?P<board_name>[A-Za-z]+)/(?P<page>\d+).json$',
        board_page_api, name="board_page_api"
    ),
    url(
        r'^(?P<board_name>[A-Za-z]+)/res/(?P<thread_id>\d+).json$',
        board_thread_api, name="board_thread_api"
    ),
    # API END
    url(
        r'^(?P<board_name>[A-Za-z]+)/(?P<current_page>\d+).html$',
        render_board,
        name="board_page"
        ),
    url(
        r'^(?P<board_name>[A-Za-z]+)/catalog.html$',
        render_catalog,
        name="catalog"
        ),
    url(
        r'^(?P<board_name>[A-Za-z]+)/'
        r'(?P<media_type>(src|thumb))/(?P<path>.*)$',
        get_media,
        name='media'
        ),
    url(r'^about/', include('django.contrib.flatpages.urls')),
]
