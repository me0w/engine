# coding: utf-8

"""handle with user forms (in static mode)"""

from subprocess import call
from tempfile import NamedTemporaryFile
from datetime import datetime
from ipaddress import ip_address
import re
import os
import hashlib
import requests

import GeoIP
from twitter import Twitter, OAuth
import simplejson as json
from decouple import config as config_

from django.forms import ModelForm
from django.urls import reverse
from django.core.files.uploadedfile import UploadedFile
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from PIL import Image
from imagekit import ImageSpec
from imagekit.processors import ResizeToFit

from config.settings import config
from config.settings import MEDIA_ROOT
from posts.models import Post, Board, Ban, BannedFile
from precise_bbcode.bbcode import get_parser


class PostForm(ModelForm):
    """
    form for user posts
    """

    def process(self, board, _ip, thread):
        """
        Add new post/thread.

        :param self: form that needs to handle
        :param board: thread or reply board
        :param _ip: ip of poster
        :param thread: thread id if we work with reply
        :return: True if form is valid and processed
        """
        for banned in Ban.current_banned():
            if ip_address(_ip) >= banned[0] and ip_address(_ip) <= banned[1]:
                raise Exception('IP was banned!')
        data = ['name', 'subject', 'email', 'body', 'password', 'op_stickers']
        [name, subject, email, body, password, stickers] = [
            self.cleaned_data[_] for _ in data]
        time = timezone.now()
        timestamp = datetime.timestamp(time)
        check_errors(thread, body, self.files)
        if thread:
            op_post = Post.objects.get(board__uri=board, id=thread)
            if op_post.locked:
                raise Exception('Thread was locked!')
            replies = Post.objects.filter(thread=op_post.id).count()
            if not email == 'sage' and replies < 250:
                op_post.bump = time
                op_post.save()
        if len(self.files) == 0:
            files = []
        else:
            try:
                files = handle_files(self.files, str(timestamp), board)
                if not files:
                    raise Exception('There are no files!')
            except Exception as e:
                raise Exception(str(e))
        _board = Board.objects.get(uri=board)
        _board.posts += 1
        _board.save()
        gi = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)
        country_code = gi.country_code_by_addr(_ip)
        nomarkup = f'{body}\n<tinyboard flag>{country_code}</tinyboard> \
            \n<tinyboard proxy>{_ip}</tinyboard>'
        body = markup(body, board) if len(body) else ''
        new_post = Post.objects.create(
            id=_board.posts,
            time=time,
            board=Board.objects.get(uri=board),
            sage=True if email == 'sage' else False,
            cycle=False,
            locked=False,
            sticky=False,
            name=name,
            num_files=len(files),
            subject=subject,
            op_stickers=stickers,
            email=email,
            body=body,
            files=json.dumps(files),
            body_nomarkup=nomarkup,
            password=password,
            ip=_ip,
            thread=thread,
            bump=time
        )
        new_post.save()
        tweet_thread(thread, new_post.id, _board.uri)
        return new_post.id

    class Meta(object):
        """
        meta class for ModelForm
        """
        model = Post
        exclude = ['time', 'sage', 'cycle', 'locked', 'sticky', 'ip', 'board']


def tweet_thread(thread, id, board):
    if thread is None:
        envs = ['TOKEN', 'TOKEN_SECRET', 'CONSUMER_KEY', 'CONSUMER_SECRET']
        credits = [config_(_, default=None) for _ in envs]

        if all(credits):
            account = Twitter(auth=OAuth(*credits))
            url = reverse('thread', args=[board, id])
            new_status = f'https://www.kropyva.ch{url}'
            account.statuses.update(status=new_status)


def spam(text):
    "Check text for spam/flood."
    for word in config['wordfilter']:
        matches = re.findall(word, text, re.IGNORECASE)
        if matches:
            return True
    return False


def handle_files(files, time, board):
    """
    Check and save files.

    :param files: files fot handling
    :param time: current time
    :param board: post's board
    :return: json list of files features
    """
    _files = []
    for file in files.items():
        size = file[1].size
        if size > config['max_filesize']:
            raise Exception('File is too big!')
        name = file[1].name
        ext = name.split('.')[-1]
        if not ext.lower() in config['allowed_ext']:
            raise Exception("File's extension is not allowed!")

        # file saving
        index = file[0].replace('file', '')  # equal 0 for first file and so on
        path = choose_path(board, 'src', time, ext, index)

        with open(path, 'wb+') as destination:
            for chunk in file[1].chunks():
                destination.write(chunk)

        # TODO: Refactor all this hell

        if ext.lower() == 'webm':
            with NamedTemporaryFile() as webm_preview:
                preview_path = webm_preview.name + '.png'
                call(["ffmpeg", "-i", path, "-vframes", "1", preview_path])
            with open(preview_path, 'rb+') as webm_thumb:
                with UploadedFile(file=webm_thumb) as preview:
                    thumb = make_thumb(preview)
        else:
            preview_path = path
            thumb = make_thumb(file[1])

        with Image.open(preview_path) as image:
            image_width = image.width
            image_height = image.height
            print(image_height, image_width)

        path = choose_path(board, 'thumb', time, 'png', index)

        with open(path, 'wb+') as destination:
            destination.write(thumb.read())

        hash = hashlib.sha256(open(path, 'rb').read()).hexdigest()

        try:
            BannedFile.objects.get(hash=hash)
            raise Exception('File was banned!')
        except BannedFile.DoesNotExist:
            pass

        with Image.open(path) as thumb:
            thumb_width = thumb.width
            thumb_height = thumb.height
            print(thumb_height, thumb_width)

        filename = f'{time}-{index}.{ext}'

        file_data = {
            "name": name,
            "type": 0,  # content_type,
            "tmp_name": ".",  # ???
            "error": 0,
            "size": size,
            "spoiler": False,
            "filename": name,
            "extension": ext,
            "file_id": time,
            "file": filename,
            "thumb": f'{time}-{index}.png',
            "is_an_image": 0,  # content_type.split('/')[0] == 'image',
            "hash": hash,
            "width": image_width,
            "height": image_height,
            "thumbwidth": thumb_width,
            "thumbheight": thumb_height,
            "file_path": f'{board}/src/{filename}',
            "thumb_path": f'{board}/thumb/{time}-{index}.png'
        }
        _files.append(file_data)
    return _files


def make_thumb(_file):
    thumb_generator = Thumbnail(source=_file)
    thumb = thumb_generator.generate()
    return thumb


def choose_path(board, _type, time, ext, index):
    """
    Form a system path for file.

    :param board: file's board
    :param _type: type of file (src/thumb)
    :param time: current time
    :param ext: extension
    :param index: file's index in the form
    :return: path string
    """
    directory = f'{MEDIA_ROOT}{_type}/{board}/'
    file = f'{time}-{index}.{ext}'
    return directory+file


def markup(body, board=None, process_links=True):
    """
    Generate a markup for text.

    :param body: text for processing
    :param board: posts board
    :return: markuped text
    """
    bb_parser = get_parser()
    body = bb_parser.render(body if body else '')
    strings = body.split('<br />')
    respond = []
    for string in strings:

        # quotation
        string = process_markup(
            r"^(?P<quote_mark>&gt;)(?P<text>(?!&gt;).+)",
            '<span class="quote">&gt;{0}</span>',
            string
        )
        # reply's

        # replies
        if board:
            string = re.sub(
                r"(?P<reply>&gt;&gt;)(?P<id>\d+)",
                lambda x: rep(x, board),
                string)

        # bold
        string = process_markup(
            r"\*\*(?P<text>[^\*\*]+)\*\*",
            '<strong>{0}</strong>',
            string
        )

        # italic
        string = process_markup(
            r"\*(?P<text>[^\*]+)\*",
            '<em>{0}</em>',
            string
        )

        # underline
        string = process_markup(
            r"\_\_(?P<text>[^\_\_]+)\_\_",
            '<u>{0}</u>',
            string
        )

        # strike
        string = process_markup(
            r"~~(?P<text>[^~~]+)~~",
            '<strike>{0}</strike>',
            string
        )

        # spoiler
        string = process_markup(
            r"\%\%(?P<text>[^\%\%]+)\%\%",
            '<span class="spoiler">{0}</span>',
            string
        )

        link_type = None

        if process_links:

            if re.match(r'.+soundcloud\.com/.+/.+', string):
                link_type = 'soundcloud'
            elif re.match(r'.+youtube\.com/watch\?v=.+', string):
                link_type = 'youtube'

            def title(video_id):
                url = f'https://www.youtube.com/watch?v={video_id}'
                req = requests.get(url)
                try:
                    match = re.search(
                            r'<title>(?P<title>.*) - YouTube<\/title>',
                            req.text
                            )
                    return match.group('title')
                except Exception:
                    pass

            # soundcloud links
            if link_type == 'soundcloud':
                inp = re.search(r'<a href="(?P<link>.*)">(?P<title>.*)</a>', string)
                page = requests.get(inp.group('link')).content.decode('UTF-8')
                title = re.search(r'<title>(?P<title>.*) \| Free Listening on SoundCloud</title>', page)\
                    .group('title')
                string = re.sub('>.+</a>', f'>{title}</a>', string)
                sc_iframe = '<iframe width="100%" height="100" scrolling="no" frameborder="no" allow="autoplay" ' \
                            'src="https://w.soundcloud.com/player/?url=' + \
                            inp.group('link') + \
                            '&color=%236b86b2&auto_play=false&hide_related=false&show_comments=false' \
                            '&show_user=false&show_reposts=false&show_teaser=true"></iframe>'

            # youtube links
            inp = re.compile(r'<a href=\"https://www\.youtube\.com/watch\?v\='
                             r'(?P<vid_id>.{11})[\?t=[\dsmh]*]?\">'
                             r'(?P<link>[^<]*)<\/a>')
            out = '<a href="{0}">{1}</a>'
            string = re.sub(
                inp,
                lambda m: out.format(
                    m.group('link'),
                    title(m.group('vid_id'))
                ),
                string
            )
        else:
            # unfortunately, it seems precise_bbcode gives no opportunity to
            # avoid url parsing by its parser
            inp = re.compile(r'<a href=".*">'
                             r'(?P<link>[^<]*)<\/a>')
            out = '{0}'
            string = re.sub(
                inp,
                lambda m: out.format(m.group('link')),
                string
            )

        if link_type == 'soundcloud':
            respond += [sc_iframe + '<br />' + string]
        else:
            respond += [string]      

    return '<br />'.join(respond)


class Thumbnail(ImageSpec):

    """Thumbnail settings."""

    processors = [ResizeToFit(255, 255)]
    format = 'PNG'


def check_errors(thread, body, files):
    if thread is None and len(files) == 0:
        raise Exception('Thread should contain a file!')
    if len(body) == 0 and len(files) == 0:
        raise Exception('Your post has no message and no file!')
    if spam(body):
        raise Exception('Go away, spammer!')
    if len(files) > config['max_images']:
        raise Exception('You are trying to upload too many files!')


def process_markup(regex, output, string):
    """
    Process markup for simple rules i.e. bold or cursive text.
    :param regex: regex condition
    :param output: rule for replace
    :return: processed string
    """

    def replace(match, result):
        text = match.group('text')
        return result.format(text)

    return re.sub(regex, lambda line: replace(line, output), string)


def rep(match, board):
    reply_id = match.group('id')
    try:
        post = Post.objects.get(board__uri=board, id=reply_id)
        if post:
            thread_id = post.thread if post.thread else post.id
            link = reverse('thread', args=[board, thread_id])
            if not post.thread:
                link += '#' + str(post.id)
            return f'''<a onclick="highlightReply('{reply_id}', \
            event);" href="{link}">&gt;&gt;{reply_id}</a>'''
    except ObjectDoesNotExist:
        pass
